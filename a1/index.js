// S29 Activity Solution


// Users with lette 's' in their first name or 'd' in their last name.

	db.users.find(
		{$or: 
			[{firstName: {$regex: "S", $options: "i"}}, {lastName: {$regex: "D", $options: "i"}}]
		},
		{
			firstName: 1,
			lastName: 1,
			_id: 0
		}
	);

// Users who are from 'HR' department and their age is greater than or equal to 70.

	db.users.find({ $and: [{department: "HR"}, {age: {$gte: 70}}]});

// Users with letter 'e' in their first name and has an age of less than or equal to 30.

	db.users.find({ $and: [{firstName: {$regex:"E", $options: "i"}}, {age: {$lte: 30}}]});