// Query Operators and Field Projection

/*
	MongoDB - Query Operators
	Expand Queries in MongoDB using Query Operators
*/

/*
	Overview: 
		Query Operators
			- Definitions
			- Importance

		Types of Query Operators:
			1. Comparison Query Operators
				1.1 Greater than
				1.2 Greater than or equal to
				1.3 Less than
				1.4 Less tha or equal to
				1.5 Not equal to
				1.6 In
			2. Evaluation Query Operators
				2.1 Regex
					2.1.1 Case sensitive query
					2.1.2 Case insensitive query
			3. Logical Query Operators
				3.1 OR
				3.2 AND

		Field Projections
			1. Inclusion
				1.1 Returning specific fields in embedded documents
				1.2 Exception to the inclusion rule
				1.3 Slice Operators
			2. Exclusion
				2.1 Excluding specific fields in embedded documents
*/

/*
	What does Query Operator Mean?
		- A 'Query' is a request for data from a database.
		- An 'Operator' is a symbol that represents an action or a process
		- Putting them together, they mean the things that we can do on our queries using certain operators
*/
/*
	Why do we need to study Query Operators?
		- Knowing Query Operators will enable us to create queries that can do more that what simple operators operators do. (We can do more than what we do in simple CRUD Operations).

		- For example, in our CRUD Operations discussion, we have discussed find using specific value inside its single or multiple parameters. When we know query operators, we may look for records that are more specific.
*/


// 1. Comparison Query Operators
	/*
		Includes: 
			- Greater than
			- Greater than or equal to
			- Less than
			- Less tha or equal to
			- Not equal to
			- In
	*/

	// Greater Than: '$gt'
		/*
			- '$gt' finds documents that have fields numbers that are greater than a specified value.

			Syntax:
				db.collectionName.find({field: {$gt: value}});
		*/

			db.users.find({age: {$gt: 76}});

	// Greater than or equal to: '$gte'
		/*
			- '$gte' finds documents that have field number values that are greater than or equal to a specified value.

			Syntax:
				db.collectionName.find({field: {$gte: value}});
		*/

			db.users.find({age: {$gte: 76}});

	// Less than: '$lt'
		/*
			- '$lt' finds documents that have field number value less than the specified value

			Syntax:
				db.collectionName.find({field: {$lt: value}});
		*/

			db.users.find({age: {$lt: 65}});

	// Less than or equal to: '$lte'
		/*
			- '$lte' finds documents that have field number values that are less than or equal to a specified value.

			Syntax:
				db.collectionName.find({field: {$lte: value}});
		*/

			db.users.find({age: {$lte: 65}});

	// Not equal to: '$ne'
		/*
			- '$ne' finds documents that have field number values that are not equal to a specified value.

			Syntax: 
				db.collectionName.find({field: {$ne: value}});
		*/

			db.users.find({age: {$ne: 65}});

	// In Operator: '$in'
		/*
			- '$in' finds documents with specific match criteria on one field using different values

			Syntax: 
				db.collectionName.find({field: {$in: value}});
		*/

			db.users.find({lastName: {$in: ['Hawking', 'Doe']}});
			db.users.find({courses: {$in: ['HTML', "React"]}});


// Evaluation Query Operators
	//  Evaluation operators return data based on evaluations of either individual fields or the entire collection's documents

	// regex operator: '$regex'
		/*
			- RegEx is short for regular expression.
			- They are called regular expressions because they are based on regular languages.
			- RegEx is used for matching strings.
			- It allows us to find documents that match a specific string pattern using regular expressions.
		*/

	// Case sensitive query
		/*
			Syntax
				db.collectionName.find({field: {$regex: 'pattern'}});
		*/
			db.users.find({lastName: {$regex: 'A'}});

	// Case insensitive query
		/*
			- We can run case-insensitive queries by utilizing the 'i' option
			Syntax
				db.collectionName.find({field: {$regex: 'pattern', $options: 'i'}});
		*/
			db.users.find({lastName: {$regex: 'A', $options: '$i'}});

	/*
		Mini-Activity #1
			- Find users with letter 'e' in their first name.
			- Make sure to use case insensitive query
	*/
		db.users.find({firstName: {$regex:'E', $options: 'i'}});


// Logical Query Operators
	
	// OR Operator: '$or'
		/*
			- '$or' finds documents that match a single criteria from multiple provided search criteria.

			Syntax:
				db.collectionName.find({$or: [{fieldA: valueA}, fieldB: valueB]});
		*/

			db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});
			db.users.find({$or: [{firstName: "Neil"}, {age: {$gt:30}]});

	// AND Operator: '$and'
		/*
			- '$and' finds documents matching multiple criteria in a single field.

			Syntax:
				db.collectionName.find({$and: [{fieldA: valueA}, fieldB: valueB]});
		*/

			db.users.find({$and: [{age: {$ne: 82}, {age: {$ne: 76}}}]});

	/*
		Mini-Activity #2
			- Find users with letter 'e' in their first name and has an age of less than or equal to 30.
	*/
		db.users.find({ $and: [{firstName: {$regex:'E', $options: 'i'}}, {age: {$lte: 30}}]});


// Field Projections
	/*
		- By default, MongoDB returns the whole documents, especially, when dealing with complex documents.
		- But sometimes, it is not helpful to view the whole documents, especially when dealing with the complex documents
		- To help with readability of the values returned (or sometimes, because of security reasons), we include or exclude some fields.
		- In other words, we project our selected fields.
	*/
	
	// Inclusion
		/*
			- Allows us to include/add specific fields only when retrieving documents.
			- The value denoted is '1' to indicate that the field is being included.
			- We cannot do exclusion on field that uses inclusion projection

			Syntax:
				db.collectionName.find({criteria}, {field: 1});
		*/
			db.users.find({firstName: 'Jane'});
			db.users.find(		
				{
					firstName: "Jane"
				},
				{
					firstName: 1,
					lastName: 1,
					"contact.phone":1
				}
			);

		// Returning specific fields in embedded documents
			//  The double quotations are important.

				db.users.find(		
					{
						firstName: "Jane"
					},
					{
						firstName: 1,
						lastName: 1,
						"contact.phone":1
					}
				);

		// Exception to the inclusion rule: Suppressing the ID field
			/*
				- Allows us to exclude the "_id" field when retrieving documents
				- When using field projection, field inclusion and exclusion may not be used at the same time.
				- Excluding the '_id' field is the ONLY exception to this rule

				Syntax:
					db.collectionName.find({criteria}, {_id: 0});
			*/

				db.users.find(		
					{
						firstName: "Jane"
					},
					{
						firstName: 1,
						lastName: 1,
						contact:1,
						_id: 0
					}
				);
	
		// Slice Operators: '$slice'
			// $slice operator allows us to retrieve only 1 element that matches the search criteria.

				// To demonstrate, let us first insert and view an array

					db.users.insertOne({
						namearr:[
							{
								namea: "Juan"
							},
							{
								nameb: "tamad"
							}
						]	
					});

					db.users.find({
						namearr:
						{
							namea: "Juan"
						}
					});

				// Now, let us use the slice operator
				db.users.find(
					{ "namearr": 
						{ 
							namea: "Juan" 
						} 
					}, 
						{ namearr: 
							{ $slice: 1 } 
						}
				);

	/*
		Mini-activity #3
			- Find users with letter 's' on their first names
			- Show only the firstName and the lastName fields and hide the id_field
	*/

		db.users.find(
			{
				firstName: {$regex: "S", $options: "i"}
			},
			{
				firstName: 1,
				lastName: 1,
				_id: 0
			}
		);

	// Exclusion
		/*
			- allows us to exclude or remove specific fields when retrieving documents
			- The value provided is zero to denote that the field is being included.

			Syntax:
				db.collectionName.find({criteria}, {field: 0})
		*/

			db.users.find(		
				{
					firstName: "Jane"
				},
				{
					contact: 0,
					department: 0
				}
			);


		// Excluding/Suppresing specific fields in Embedded Documents

			db.users.find(
				{
					firstName: "Jane"
				},
				{
					"contact.phone": 0
				}
			);